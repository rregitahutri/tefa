package com.example.tefa.data.remote

import com.example.tefa.data.model.UserItem
import javax.inject.Inject

class Repository @Inject constructor(private val service: ApiService) {

    suspend fun getUserList() : List<UserItem> {
        return service.getUserList(2).data
    }
}